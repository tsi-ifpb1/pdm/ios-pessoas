//
//  ViewController.swift
//  Pessoas
//
//  Created by Mauricio Pereira on 14/12/20.
//  Copyright © 2020 Mauricio Pereira. All rights reserved.
//

import UIKit

class FormViewController: UIViewController {
    @IBOutlet weak var tfNome: UITextField!
    @IBOutlet weak var tfIdade: UITextField!
    
    var pessoa: Pessoa?
    var posicao: Int?
    var cadastro: Cadastro?
    
    override func viewDidLoad() {
        if let pessoa = self.pessoa {
            self.tfNome.text = pessoa.nome
            self.tfIdade.text = String(pessoa.idade)
        }
    }
    
    @IBAction func salvar(_ sender: Any) {
        
        if let pos = self.posicao {
            self.pessoa?.nome = self.tfNome.text!
            self.pessoa?.idade = Int(self.tfIdade.text!)!
            
            self.cadastro?.update(index: pos, pessoa: self.pessoa!)
        } else {
            let nome = self.tfNome.text
            let idade = Int(self.tfIdade.text!)
            let pessoa = Pessoa(nome: nome!, idade: idade!)
            
            self.cadastro?.add(pessoa: pessoa)
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
}

