//
//  ListarTableViewController.swift
//  Pessoas
//
//  Created by Mauricio Pereira on 14/12/20.
//  Copyright © 2020 Mauricio Pereira. All rights reserved.
//

import UIKit

class ListarTableViewController: UITableViewController {
    
    var cadastro = Cadastro()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.cadastro.add(pessoa: Pessoa(nome: "Primeira", idade: 1))
        self.cadastro.add(pessoa: Pessoa(nome: "Segunda", idade: 2))
        self.cadastro.add(pessoa: Pessoa(nome: "Terceira", idade: 3))
        self.cadastro.add(pessoa: Pessoa(nome: "Quarta", idade: 4))

        self.navigationItem.leftBarButtonItem = self.editButtonItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        print(self.cadastro.get())
        self.tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cadastro.count()
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "celula", for: indexPath)

        let pessoa = self.cadastro.get(index: indexPath.row)
        
        cell.textLabel?.text = pessoa.nome
        cell.detailTextLabel?.text = String(pessoa.idade)
        
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.cadastro.del(index: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }

    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
        self.cadastro.mov(from: fromIndexPath.row, to: to.row)
    }
    
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pessoa = self.cadastro.get(index: indexPath.row)
        
//        let janela = UIAlertController(title: "Atenção", message: pessoa.description, preferredStyle: UIAlertController.Style.alert)
//        janela.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
        
        let formvc = self.storyboard?.instantiateViewController(identifier: "form") as! FormViewController
        formvc.pessoa = pessoa
        formvc.posicao = indexPath.row
        
        self.navigationController?.pushViewController(formvc, animated: true)
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "listar_form") {
            let fvc = segue.destination as! FormViewController
            fvc.cadastro = self.cadastro
        }
    }

}
