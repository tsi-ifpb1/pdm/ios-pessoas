//
//  Pessoa.swift
//  Pessoas
//
//  Created by Mauricio Pereira on 14/12/20.
//  Copyright © 2020 Mauricio Pereira. All rights reserved.
//

import Foundation

class Pessoa: NSObject {
    var nome: String
    var idade: Int
    
    override var description: String {
        return "\(self.nome) - \(self.idade)"
    }
    
    init(nome: String, idade: Int) {
        self.nome = nome
        self.idade = idade
    }
}
